% Возвращает матрицу со случайными почасовыми расписаниями на месяц длиной size.
% size - количество случайных расписаний, которое нужно построить.
% days_count - количество дней в месяце.
% hours - количество часов работы в месяц для одного сотрудника.
% H - матрица с расписанием выходных дней.
function res = rand_schedule(size, days_count, hours, H)
  res = [];
  for i = 1 : size
    do
      next = rand_one_schedule(days_count, hours, H);
    until (~ismember(next', res', 'rows'))
    res = [res, next];
  endfor
endfunction
