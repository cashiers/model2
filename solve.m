function [f, x, x1, x2, extr] = solve(days_count, day_length, const1, B, H, max_error = 0, max_iterations = Inf, m1 = 100, round_const = 0.18)
  a1 = 4;
  a2 = 8;
  a3 = 12;
  A0 = zeros(day_length, 1);
  A1 = day_schedule(day_length, a1);
  A2 = day_schedule(day_length, a2, 5);
  A3 = day_schedule(day_length, a3, 7);
  A = [A1, A2, A3];
  const_A_size = columns(A) * days_count;
  Wa = zeros(day_length * days_count, const_A_size);
  for i = 1 : days_count
    Wa((i - 1) * day_length + 1 : i * day_length, (i - 1) * columns(A) + 1 : i * columns(A)) = -A;
  endfor
  L = zeros(3, columns(A));
  L(1, 1 : columns(A1)) = 1;
  L(2, columns(A1) + 1 : columns(A1) + columns(A2)) = 1;
  L(3, columns(A1) + columns(A2) + 1 : columns(A)) = 1;
  Wl = zeros(3 * days_count, const_A_size);
  for i = 1 : days_count
    Wl((i - 1) * 3 + 1 : i * 3, (i - 1) * columns(A) + 1 : i * columns(A)) = L;
  endfor
  FULL = rand_schedule(m1, days_count, const1, H);
  W = [[Wa; Wl], [zeros(day_length * days_count, m1); -FULL]];
  Bwave = [-B; zeros(3 * days_count, 1)];
  CTYPE = repmat("U", 1, (day_length + 3) * days_count);
  A2x = zeros(days_count, days_count * 3);
  A2x(:, 1 : 3 : days_count * 3) = eye(days_count);
  A2x(:, 2 : 3 : days_count * 3) = eye(days_count);
  A2x(:, 3 : 3 : days_count * 3) = eye(days_count);
  A2x = [repmat([a1, a2, a3], 1, days_count); A2x];
  LB2 = zeros(1, 3 * days_count);
  UB2 = repmat(Inf, 1, 3 * days_count);
  CTYPE2 = repmat("S", 1, days_count + 1);
  VARTYPE2 = repmat("I", 3 * days_count, 1);
  LB3 = zeros(1, columns(A) + 3);
  UB3 = repmat(Inf, 1, columns(A) + 3);
  CTYPE3 = [repmat("L", 1, day_length), repmat("U", 1, 3)];
  VARTYPE3 = repmat("I", columns(A) + 3, 1);
  LB4 = zeros(1, columns(A) + day_length);
  UB4 = repmat(Inf, 1, columns(A) + day_length);
  VARTYPE4 = repmat("I", columns(A) + day_length, 1);
  vars = m1;
  iteration = 0;
  extr.iter_size = [];
  extr.iter_f = [];
  extr.max_qc = [];
  do
    extr.iter_size = [extr.iter_size, vars];
    [x, f, status, extra] = glpk([zeros(1, const_A_size), ones(1, vars)], W, Bwave, zeros(1, columns(W)), repmat(Inf, 1, columns(W)), CTYPE);
    extr.iter_f = [extr.iter_f, f];
    lambda = extra.lambda(day_length * days_count + 1 : (day_length + 3) * days_count)';
    max_qc = [];
    new_cols = [];
    for i = 1 : rows(H)
      B2 = [const1; ones(days_count, 1) - H(i, :)'];
      [x2, f2] = glpk(-lambda, A2x, B2, LB2, UB2, CTYPE2, VARTYPE2, -1);
      max_qc = [max_qc, f2];
      if f2 > 1 + max_error
        new_cols = [new_cols, x2];
      endif
    endfor
    extr.max_qc = [extr.max_qc; max_qc];
    if vars + columns(new_cols) > m1
      to_remove = find(x(const_A_size + 1 : columns(W)) == 0);
      if vars + columns(new_cols) - m1 >= rows(to_remove)
        W(:, to_remove + const_A_size) = [];
      else
        qcs = lambda * W(day_length * days_count + 1 : (day_length + 3) * days_count, const_A_size + 1 : columns(W));
        for i = 1 : vars + columns(new_cols) - m1
          [~, to_remove] = min(qcs);
          qcs(:, to_remove) = [];
          W(:, to_remove + const_A_size) = [];
        endfor
      endif
    endif
    if columns(new_cols)
      W = [W, [zeros(day_length * days_count, columns(new_cols)); -new_cols]];
    endif
    vars = columns(W) - const_A_size;
    iteration++;
  until !columns(new_cols) || iteration >= max_iterations
  extr.iterations = iteration;
  FULL = -W(day_length * days_count + 1 : (day_length + 3) * days_count, const_A_size + 1 : columns(W));
  xCircle = x(const_A_size + 1 : columns(W));
  to_remove = find(xCircle == 0);
  FULL(:, to_remove) = [];
  xCircle(to_remove) = [];
  TEST = FULL * xCircle;
  TESTint = round(TEST - round_const);
  vars = columns(FULL);
  [xStar, f] = glpk(ones(1, vars), FULL, TESTint, zeros(1, vars), repmat(Inf, 1, vars), repmat("L", 1, 3 * days_count), repmat("I", vars, 1));
  TESTnew = FULL * xStar;
  x = zeros(day_length * days_count, f);
  x1 = [];
  x2 = [];
  for i = 1 : days_count
    C3 = [zeros(1, columns(A)), 1, 1, 1];
    A3x = [[A, zeros(day_length, 3)]; [L, -eye(3)]];
    B3 = [B((i - 1) * day_length + 1 : i * day_length); TESTnew((i - 1) * 3 + 1 : i * 3)];
    [x3, f3] = glpk(C3, A3x, B3, LB3, UB3, CTYPE3, VARTYPE3);
    x1 = [x1, x3(columns(A) + 1 : rows(x3))];
    if f3 == 0
      x2 = [x2, zeros(day_length, 1)];
    else
      C4 = [zeros(1, columns(A)), ones(1, day_length)];
      A4 = [[A, eye(day_length)]; [L, zeros(3, day_length)]];
      x3 = glpk(C4, A4, B3, LB4, UB4, CTYPE3, VARTYPE4);
      x2 = [x2, x3(columns(A) + 1 : rows(x3))];
    endif
    cashier = 1;
    for j = 1 : columns(FULL)
      for k = 1 : xStar(j)
        if (FULL((i - 1) * 3 + 1, j))
          idx = find(x3(1 : columns(A1)), 1);
          if isempty(idx)
            idx = 1;
          endif
          x((i - 1) * day_length + 1 : i * day_length, cashier) = A1(:, idx);
          x3(idx)--;
        elseif (FULL((i - 1) * 3 + 2, j))
          idx = find(x3(columns(A1) + 1 : columns(A1) + columns(A2)), 1);
          if isempty(idx)
            idx = 1;
          endif
          x((i - 1) * day_length + 1 : i * day_length, cashier) = A2(:, idx);
          x3(columns(A1) + idx)--;
        elseif (FULL((i - 1) * 3 + 3, j))
          idx = find(x3(columns(A1) + columns(A2) + 1 : columns(A)), 1);
          if isempty(idx)
            idx = 1;
          endif
          x((i - 1) * day_length + 1 : i * day_length, cashier) = A3(:, idx);
          x3(columns(A1) + columns(A2) + idx)--;
        endif
        cashier++;
      endfor
    endfor
  endfor
  to_remove = find(xStar == 0);
  FULL(:, to_remove) = [];
  xStar(to_remove) = [];
  xStar = xStar';
endfunction
