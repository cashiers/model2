function [xopt, fmin, errnum, extra] = lpsolve(c, A, b, lb, ub, ctype, vartype, sense, param)
  if nargin < 3 || nargin > 9
    print_usage();
    return;
  endif
  if all(size(c) > 1) || iscomplex(c) || ischar(c)
    error("lpsolve: C must be a real vector");
    return;
  endif
  nx = length(c);
  c = c(:);
  if isempty(A)
    error ("lpsolve: A cannot be an empty matrix");
    return;
  endif
  [nc, nxa] = size(A);
  if !isreal(A) || nxa != nx
    error("lpsolve: A must be a real valued %d by %d matrix", nc, nx);
    return;
  endif
  if isempty(b)
    error("lpsolve: B cannot be an empty vector");
    return;
  endif
  if !isreal(b) || length(b) != nc
    error("lpsolve: B must be a real valued %d by 1 vector", nc);
    return;
  endif
  if nargin > 3
    if isempty(lb)
      lb = zeros(nx, 1);
    elseif !isreal(lb) || all(size(lb) > 1) || length(lb) != nx
      error("lpsolve: LB must be a real valued %d by 1 column vector", nx);
      return;
    endif
  else
    lb = zeros(nx, 1);
  endif
  if nargin > 4
    if isempty(ub)
      ub = Inf(nx, 1);
    elseif !isreal(ub) || all(size(ub) > 1) || length(ub) != nx
      error("lpsolve: UB must be a real valued %d by 1 column vector", nx);
      return;
    endif
  else
    ub = Inf(nx, 1);
  endif
  if nargin > 5
    if isempty(ctype)
      ctype = repmat("S", nc, 1);
    elseif !ischar(ctype) || all(size(ctype) > 1) || length(ctype) != nc
      error("lpsolve: CTYPE must be a char valued vector of length %d", nc);
      return;
    elseif !all(ctype == "U" | ctype == "S" | ctype == "L")
      error("lpsolve: CTYPE must contain only U, S or L");
      return;
    endif
  else
    ctype = repmat("S", nc, 1);
  endif
  if nargin > 6
    if isempty(vartype)
      vartype = repmat("C", nx, 1);
    elseif !ischar(vartype) || all(size(vartype) > 1) || length(vartype) != nx
      error("lpsolve: VARTYPE must be a char valued vector of length %d", nx);
      return;
    elseif !all(vartype == "C" | vartype == "I")
      error("lpsolve: VARTYPE must contain only C or I");
      return;
    endif
  else
    vartype = repmat("C", nx, 1);
  endif
  if nargin > 7
    if isempty(sense)
      sense = 1;
    elseif ischar(sense) || all(size(sense) > 1) || !isreal(sense)
      error ("lpsolve: SENSE must be an integer value");
    elseif sense >= 0
      sense = 1;
    else
      sense = -1;
    endif
  else
    sense = 1;
  endif
  if nargin > 8
    if !isstruct(param)
      error ("lpsolve: PARAM must be a structure");
      return;
    endif
  else
    param = struct();
  endif
  lp = octlpsolve("make_lp", nc, nxa);
  octlpsolve("set_mat", lp, A);
  octlpsolve("set_rh_vec", lp, b);
  octlpsolve("set_obj_fn", lp, c);
  for i = 1 : nx
    if lb(i) != 0
      octlpsolve("set_lowbo", lp, i, lb(i));
    endif
  endfor
  for i = 1 : nx
    if ub(i) != Inf
      octlpsolve("set_upbo", lp, i, ub(i));
    endif
  endfor
  for i = 1 : nc
    if ctype(i) == "U"
      con_type = 1;
    elseif ctype(i) == "S"
      con_type = 3;
    elseif ctype(i) == "L"
      con_type = 2;
    endif
    octlpsolve("set_constr_type", lp, i, con_type);
  endfor
  for i = 1 : nx
    if vartype(i) == "I"
      octlpsolve("set_int", lp, i, 1);
    endif
  endfor
  if sense == -1
    octlpsolve("set_maxim", lp);
  endif
  % if scalemode ~= 0
  %   octlpsolve("set_scaling", lp, scalemode);
  % end
  if isfield(param, "debug") && param.debug
    octlpsolve("set_debug", lp, 1);
  endif
  if isfield(param, "verbose")
    octlpsolve("set_verbose", lp, param.verbose);
  endif
  if isfield(param, "break_at_first") && param.break_at_first
    octlpsolve("set_break_at_first", lp, 1);
  endif
  if isfield(param, "break_at_value") && (sense != -1 && param.break_at_value != Inf || sense == -1 && param.break_at_value != -Inf)
    octlpsolve("set_break_at_value", lp, param.break_at_value);
  endif
  if isfield(param, "obj_bound") && (sense != -1 && param.obj_bound != Inf || sense == -1 && param.obj_bound != -Inf)
    octlpsolve("set_obj_bound", lp, param.obj_bound);
  endif
  errnum = octlpsolve("solve", lp);
  if errnum == 0 || errnum == 1 || errnum == 11 || errnum == 12
    [fmin, xopt, extra.lambda] = octlpsolve("get_solution", lp);
  end
  octlpsolve("delete_lp", lp);
endfunction
