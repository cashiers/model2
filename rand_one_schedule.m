% Возвращает вектор-столбец со случайным расписанием на месяц.
% days_count - количество дней в месяце.
% hours - количество часов работы в месяц для одного сотрудника.
% H - матрица с расписанием выходных дней.
function res = rand_one_schedule(days_count, hours, H)
  month = rand_month(days_count, hours / 4, H);
  res = [];
  for i = 1 : days_count
    if (month(i) == 0)
      res = [res; 0; 0; 0];
    elseif (month(i) == 1)
      res = [res; 1; 0; 0];
    elseif (month(i) == 2)
      res = [res; 0; 1; 0];
    elseif (month(i) == 3)
      res = [res; 0; 0; 1];
    endif
  endfor
endfunction
