% Возвращает случайное целое число в отрезке [a; b].
function res = int_rand(a, b)
  if (a == b)
    res = a;
  else
    res = round(unifrnd(a, b));
  endif
endfunction
